<?php 
    function is_simple($val) {

        $sum_of_dviders = 0;
        for ($i=$val; $i >= 1; $i--) { 
    
            if($val % $i == 0 ) {
                $sum_of_dviders++;
            }
        }
    
        if($sum_of_dviders == 2) {
            return true;
        } else {
            return false;
        }
    
    }
    
    function find_e($fi) {
        for($i=1; $i < $fi; $i ++) {
            if(is_simple($i) & $fi % $i != 0) {
                return $i;
            }
        }
        return 0;
    }

    function get_d($e, $fi) {
        for ($i=$e+1; $i < $e + 2000; $i++) { 
            if(($i * $e) % $fi === 1) {
                return $i;
            }
        }
        return 0;
    }

    function encode($val, $e, $mod) {
        return pow(intval($val), $e) % $mod;
    }

    function decode($val, $d, $mod) {
        return var_dump(pow(intval($val), $d)) % $mod;
    }


    function encode_string($string, $e, $mod) {
        $splitted = str_split($string);
        $mapped = array_map(function($char) use ($e, $mod) {
            $sequintial_index = ord(strtoupper($char)) - ord('A') + 1;
            return pow(intval($sequintial_index), $e) % $mod;
        }, $splitted);
        return base64_encode(implode(',', $mapped));
        return implode(',', $mapped);

    }

    function decode_string($string, $d, $mod) {
        $decoded = base64_decode($string);
        $splitted = explode(',', $decoded);
        
        $mapped = array_map(function($char) use($d, $mod) {
            $sequintial_index = pow(intval($char), $d) % $mod;
            return chr($sequintial_index + ord('A') - 1);
        }, $splitted);

        return implode($mapped);
    }

    $prime_number = array_values(array_filter(range(0, 10), "is_simple"));
    $p = $prime_number[count($prime_number) -3];
    $q = $prime_number[count($prime_number) -1];
    $mod = $p * $q;
    $fi = ($p-1) * ($q - 1);
    $e = find_e($fi);
    $d = get_d($e, $fi);


    $private_key = base64_encode("{$mod}, {$d}");
    $public_key = base64_encode("{$e}, {$mod}");
?>